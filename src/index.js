import ContactSupportForm from './ContactSupportForm.vue'

export default {
  install: function (Vue) {
    Vue.component(ContactSupportForm.name, ContactSupportForm)
  }
}
