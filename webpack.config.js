const path = require('path');
const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

let environment = {
  entry: {
    'vuejs-contact-form.js': './src/index.js',
    'vuejs-contact-form.min.js': './src/index.js',
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: '[name]',
    library: {
      root: "vuejsContactForm",
      amd: "vuejs-contact-form",
      commonjs: "vuejs-contact-form",
    },
    libraryTarget: 'umd',
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        include: [ path.resolve(__dirname, './src') ],
        loader: 'babel-loader',
        query: {
          presets: [ '@babel/preset-env' ]
        }
      }
    ]
  },
  stats: {
    colors: true
  },
  plugins: [
    new VueLoaderPlugin()
  ]
};

if (process.env.NODE_ENV === 'production') {
  // environment.plugins.push(
  //   new webpack.optimize.ModuleConcatenationPlugin()
  // );

  environment.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      include: /\.min\.js$/,
      minimize: true,
      compress: {
        warnings: false
      },
      output: {
        comments: false
      },
      sourceMap: true
    })
  );
}

module.exports = environment
